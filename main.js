
const clickedKeyboardButton = document.getElementById("inputDisplay");

const buttons = document.querySelectorAll('button');

function specificCharacter() {
  const userInputSymbols = document.getElementById('inputDisplay').value; //Reads and gets the users input in the textfield

  const allowedSymbols = /[0-9*/+-.(^)!]/;
  const found = userInputSymbols.match(allowedSymbols);
  if(found){
    /*solve();*/       //Make this function
  }else{
    alert("No letters");
  }
}


function solve(event){
  const userInputSymbols = document.getElementById('inputDisplay').value;
  const solveButton = event.target.value;

  //FIX SUCH THAT YOU CAN CALCULATE WITH NEGATIVE NUMBERS!

  /*function roundIt(userInputSymbols){
    Number.parseFloat(result).toPrecision(3);
  }*/

  //Move this func inside if statements bellow, because otherwise it cannot recognize var1 and var2
  /*function precedingRules(){
    if(userInputSymbols.includes('(' + var1 + ')') || userInputSymbols.includes('(' + var2 + ')')){

    }
  }*/

  if(solveButton === 'Calculate')
  {
    if(userInputSymbols.includes('+') && userInputSymbols !== '')
    {
      const var1 = parseFloat(userInputSymbols.slice(userInputSymbols.indexOf('+') + 1), 10);
      const var2 = parseFloat(userInputSymbols.slice(userInputSymbols.indexOf('+') - userInputSymbols.size), 10);
      const result = var1 + var2;

      if(userInputSymbols === var1 + '+' + var2){
        alert(userInputSymbols + ' = ' + result);
      }else{
        alert("Invalid input");
      }
    }else if(userInputSymbols.includes('-'))
    {
      const var1 = parseFloat(userInputSymbols.slice(userInputSymbols.indexOf('-') + 1), 10);
      const var2 = parseFloat(userInputSymbols.slice(userInputSymbols.indexOf('-') - userInputSymbols.size), 10);
      const result = var2 - var1;
      result1 = Number.parseFloat(result).toPrecision(3)


      if(userInputSymbols === var2 + '-' + var1){
        alert(userInputSymbols + ' = ' + result1);
      }else{
        alert("Invalid input");
      }
    }else if(userInputSymbols.includes('*')){
      const var1 = parseFloat(userInputSymbols.slice(userInputSymbols.indexOf('*') + 1), 10);
      const var2 = parseFloat(userInputSymbols.slice(userInputSymbols.indexOf('*') - userInputSymbols.size), 10);
      const result = var1 * var2;
      result1 = Number.parseFloat(result).toPrecision(3)


      if(userInputSymbols === var2 + '*' + var1){
        alert(userInputSymbols + ' = ' + result1);
      }else{
        alert("Invalid input");
      }
    }else if(userInputSymbols.includes('/')){
      const var1 = parseFloat(userInputSymbols.slice(userInputSymbols.indexOf('/') + 1), 10);
      const var2 = parseFloat(userInputSymbols.slice(userInputSymbols.indexOf('/') - userInputSymbols.size), 10);
      const result = var2 / var1;
      result1 = Number.parseFloat(result).toPrecision(3)


      if(userInputSymbols === var2 + '/' + var1){
        alert(userInputSymbols + ' = ' + result1);
      }else{
        alert("Invalid input");
      }
    }else if(userInputSymbols.indexOf('!') > -1){
      const factVal = parseInt(userInputSymbols.slice(userInputSymbols.indexOf('!') + 1), 10)

      function factorial(factVal){
        return factVal != 1 ? factVal * factorial(factVal - 1) : 1;
      }

      if(userInputSymbols === "!" + factVal){
        alert("Result of your input = " + factorial(factVal));
      }else{
        alert("Invalid input");
      }
    }else if(userInputSymbols.indexOf('^') > -1){
      const base = parseFloat(userInputSymbols.slice(0, userInputSymbols.indexOf('^')), 10);
      const exponent = parseFloat(userInputSymbols.slice(userInputSymbols.indexOf('^') + 1), 10);
      const powerResult = Math.pow(base,exponent);

      if(userInputSymbols === base + '^' + exponent){
        alert('The power of: ' + base + "^" + exponent + "  = " + powerResult);
      }else{
        alert("Invalid input");
      }
    }
  }
}